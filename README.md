We offer a wide variety of surf lessons that will fit everyone's needs and budget. From private lessons, group lessons, yoga & surf packages, corporate outings, parties, and camps, we do it all! We are your one stop shop for surfing in the Charleston area.

Address: 220 West Arctic Ave, Folly Beach, SC 29439, USA
Phone: 843-813-7897
Website: http://www.islasurfschool-charleston.com
